#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <opencv2/opencv.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "constants.h"
#include "findEyeCenter.h"
#include "findEyeCorner.h"
#include "MediaPlayer.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void detectAndDisplay(cv::Mat frame);
    void findEyes(cv::Mat frame_gray, cv::Rect face, cv::Mat debugImage);
    
private:
    Ui::MainWindow *ui;
    cv::String face_cascade_name;
    cv::CascadeClassifier face_cascade;
    cv::RNG rng();
    cv::Mat debugImage;
    cv::VideoCapture capture;
    cv::Mat frame;
    bool CAMERASTATUS;
    QTimer* Ttimer;
    bool ISPLAY;
    bool ISMEDIAPLAYERPLAY;
    int mediaplayer;

public slots:
    void Proses();



private slots:
    void on_pushButton_clicked();
    void on_startservice_clicked();
};

#endif // MAINWINDOW_H
