#-------------------------------------------------
#
# Project created by QtCreator 2014-01-02T04:21:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = autopause
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp\
    findEyeCenter.cpp \
    helpers.cpp \
    findEyeCorner.cpp \
    MediaPlayer.cpp

HEADERS  += mainwindow.h\
    helpers.h \
    findEyeCenter.h \
    findEyeCorner.h \
    constants.h\
    MediaPlayer.h

FORMS    += mainwindow.ui

CONFIG += static
CONFIG += release


INCLUDEPATH += C:\\Qt\\qt-4.8.4\\include
INCLUDEPATH += C:\\openCVmingw\\install\\include

LIBS += C:\\Qt\\qt-4.8.4\lib\\libQtGui.a
LIBS += C:\\openCVmingw\\install\\lib\\libopencv_core245.dll.a
LIBS += C:\\openCVmingw\\install\\lib\\libopencv_objdetect245.dll.a
LIBS += C:\\openCVmingw\\install\\lib\\libopencv_highgui245.dll.a
LIBS += C:\\openCVmingw\\install\\lib\\libopencv_imgproc245.dll.a

