#ifndef MediaPlayer_h
#define MediaPlayer_h

#define MEDIAPLAYERCLASSIC 1
#define WINDOWSMEDIAPLAYER 2

int checkMediaPlayer();
bool isEyesLookAtTheScreen();
void sendPauseAndPlay(int typeMediaPlayer);

#endif
