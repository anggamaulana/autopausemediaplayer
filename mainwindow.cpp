#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "stdlib.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    face_cascade_name = "haarcascade_frontalface_alt.xml";
    CAMERASTATUS=false;
    ISPLAY=false;
    ISMEDIAPLAYERPLAY=false;
      capture.open(0);
      if(capture.isOpened()) {
         CAMERASTATUS=true;

  }else{
          CAMERASTATUS=false;
      }
      Ttimer = new QTimer(this);
     connect(Ttimer,SIGNAL(timeout()),this,SLOT(Proses()));

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::detectAndDisplay(cv::Mat frame){
    std::vector<cv::Rect> faces;
    //cv::Mat frame_gray;

             std::vector<cv::Mat> rgbChannels(3);
             cv::split(frame, rgbChannels);
             cv::Mat frame_gray=rgbChannels[2];

    cv::Mat temp2;
    cv::resize(frame_gray,temp2,cv::Size(211,161));

    QImage imagegrey((uchar*)temp2.data,temp2.cols,temp2.rows,temp2.step,QImage::Format_Indexed8);
    ui->graypic->setPixmap(QPixmap::fromImage(imagegrey));

    //cvtColor( frame, frame_gray, CV_BGR2GRAY );
    //equalizeHist( frame_gray, frame_gray );
    //cv::pow(frame_gray, CV_64F, frame_gray);
    //-- Detect faces

    face_cascade.detectMultiScale( frame_gray, faces, 1.05, 2, 0|CV_HAAR_SCALE_IMAGE, cv::Size(80, 80) );
  //  findSkin(debugImage);


    for( unsigned int i = 0; i < faces.size(); i++ )
    {
      cv::rectangle(debugImage, faces[i], 1234);
    }
    //-- Show what you got
    if (faces.size() > 0) {
        std::stringstream ss;
        std::string count;
        ss << faces.size();
        ss >> count;

        ui->headcount->setText(QString(count.c_str()));
        findEyes(frame_gray, faces[0],debugImage);
        //check wether pause based on eye center is activated
        if(ui->cbox_eyecenter->isChecked()==false){
            //play based on face detected
            //if media player paused then
            if(ISMEDIAPLAYERPLAY==false){
                sendPauseAndPlay(mediaplayer);
                 ui->actionmediaplayer->setText(QString("Play"));
                ISMEDIAPLAYERPLAY=true;
            }
        }else{
            //play and pause based on eye center point
            //get signal play/pause
            if(getPLAYSIGNAL()){
                //if video is paused then play video
                if(ISMEDIAPLAYERPLAY==false){
                    sendPauseAndPlay(mediaplayer);
                     ui->actionmediaplayer->setText(QString("Play"));
                    ISMEDIAPLAYERPLAY=true;
                }
            }else{
                //if video is played then pause video
                if(ISMEDIAPLAYERPLAY==true){
                    sendPauseAndPlay(mediaplayer);
                     ui->actionmediaplayer->setText(QString("Pause"));
                    ISMEDIAPLAYERPLAY=false;
                }
            }

        }

    }else{
        ui->headcount->setText("0");
        if(ISMEDIAPLAYERPLAY==true){
             sendPauseAndPlay(mediaplayer);
             ui->actionmediaplayer->setText(QString("Pause"));
             ISMEDIAPLAYERPLAY=false;
        }
    }

}

void MainWindow::Proses(){
    if(CAMERASTATUS){
        mediaplayer = checkMediaPlayer();
      if(mediaplayer!=0){

        std::string typemedia;
        if(mediaplayer==MEDIAPLAYERCLASSIC){
            typemedia="MEDIAPLAYERCLASSIC Detected";
        }else if(mediaplayer==WINDOWSMEDIAPLAYER){
            typemedia="WINDOWSMEDIAPLAYER Detected";
        }
        ui->typemediaplayer->setText(QString(typemedia.c_str()));


        if(!face_cascade.load(face_cascade_name)){
            return;
        }
//        frame = cvQueryFrame( capture );
        capture.read(frame);
        // mirror it
        cv::flip(frame, frame, 1);
        frame.convertTo(frame,-1,1.5,0);
        cv::Mat temp1;
        cv::resize(frame,temp1,cv::Size(211,161));
        frame.copyTo(debugImage);
        QImage image((uchar*)temp1.data,temp1.cols,temp1.rows,temp1.step,QImage::Format_RGB888);
        ui->normal->setPixmap(QPixmap::fromImage(image));

        // Apply the classifier to the frame
        if(!frame.empty()) {


          detectAndDisplay( frame );

        }
        else {
          printf(" --(!) No captured frame -- Break!");
          return;
        }

        cv::resize(debugImage,debugImage,cv::Size(451,341));
        QImage image3((uchar*)debugImage.data,debugImage.cols,debugImage.rows,debugImage.step,QImage::Format_RGB888);
        ui->result->setPixmap(QPixmap::fromImage(image3));
      }else{
          ui->typemediaplayer->setText(QString("Media Player Not Detected"));
          ui->result->setText(QString("Media Player Not Detected"));

      }

    }
}

void MainWindow::findEyes(cv::Mat frame_gray, cv::Rect face,cv::Mat debugImage) {
    cv::Mat faceROI = frame_gray(face);

    if (kSmoothFaceImage) {
      double sigma = kSmoothFaceFactor * face.width;
      GaussianBlur( faceROI, faceROI, cv::Size( 0, 0 ), sigma);
    }
    //-- Find eye regions and draw them
    int eye_region_width = face.width * (kEyePercentWidth/100.0);
    int eye_region_height = face.width * (kEyePercentHeight/100.0);
    int eye_region_top = face.height * (kEyePercentTop/100.0);
    cv::Rect leftEyeRegion(face.width*(kEyePercentSide/100.0),
                           eye_region_top,eye_region_width,eye_region_height);
    cv::Rect rightEyeRegion(face.width - eye_region_width - face.width*(kEyePercentSide/100.0),
                            eye_region_top,eye_region_width,eye_region_height);

    cv::Point faceCoordinat(face.x,face.y);

    //-- Find Eye Centers
    cv::Point leftPupil = findEyeCenter(faceROI,leftEyeRegion,"Left Eye",LEFTEYE,debugImage,faceCoordinat);
    cv::Point rightPupil = findEyeCenter(faceROI,rightEyeRegion,"Right Eye",RIGHTEYE,debugImage,faceCoordinat);

    rightPupil.x += rightEyeRegion.x;
    rightPupil.y += rightEyeRegion.y;
    leftPupil.x += leftEyeRegion.x;
    leftPupil.y += leftEyeRegion.y;
    // draw eye centers
    circle(faceROI, rightPupil, 3, 1234);
    circle(faceROI, leftPupil, 3, 1234);

    //draw in debug image
    leftEyeRegion.x=face.x;
    leftEyeRegion.y=face.y;
    rightEyeRegion.x=face.x;
    rightEyeRegion.y=face.y;


    rightPupil.x += rightEyeRegion.x;
    rightPupil.y += rightEyeRegion.y;
    leftPupil.x += leftEyeRegion.x;
    leftPupil.y += leftEyeRegion.y;

    // draw eye centers in debugImage
    circle(debugImage, rightPupil, 3, 1234);
    circle(debugImage, leftPupil, 3, 1234);



    //cv::Mat pupil = frame_gray(r).clone();
    //imshow("EYE",pupil);
//    imshow(face_window_name, faceROI);
    cv::Mat temp3;
    cv::resize(faceROI,temp3,cv::Size(211,161));
    QImage image2((uchar*)temp3.data,temp3.cols,temp3.rows,temp3.step,QImage::Format_Indexed8);
    ui->normal_3->setPixmap(QPixmap::fromImage(image2));
  }

void MainWindow::on_pushButton_clicked()
{

}

void MainWindow::on_startservice_clicked()
{
    if(ISPLAY==false){
        Ttimer->start();
        ui->startservice->setText(QString("Stop Service"));
         ui->status->setText(QString("Service is running"));
        ISPLAY=true;
    }else{
        Ttimer->stop();
         ui->startservice->setText(QString("Start Service"));
          ui->status->setText(QString("Service Stoped"));
         ISPLAY=false;
    }
}
