//To use sendinput must windows 200 or later
#define WINVER 0x0500

#include <windows.h>
#include <tlhelp32.h>
#include <cwchar>
#include <MediaPlayer.h>
#include <iostream>

#define WMP_PLAY 18808
#define WMP_PAUSE 18808
#define WMP_STOP 18809
#define WMP_BACK 18810
#define WMP_NEXT 18811



int checkMediaPlayer(){
    HANDLE hProcessSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL,0 );

    // Initialize the process entry structure.
    PROCESSENTRY32 ProcessEntry = { 0 };
    ProcessEntry.dwSize = sizeof(ProcessEntry);

    // Get the first process info.
    bool Return = FALSE;

    Return = Process32First( hProcessSnapShot,
                             &ProcessEntry );

    // Getting process info failed.
    if( !Return )
    {
        return 0;
    }

    int idmediaplayer=0;

    do
    {
        if(wcscmp(ProcessEntry.szExeFile,L"mpc-hc.exe")==0){
            idmediaplayer=MEDIAPLAYERCLASSIC;

        }else if(wcscmp(ProcessEntry.szExeFile,L"wmplayer.exe")==0){
            idmediaplayer=WINDOWSMEDIAPLAYER;
        }
        // check the PROCESSENTRY32 for other members.
    }
    while( Process32Next( hProcessSnapShot, &ProcessEntry ));

    CloseHandle( hProcessSnapShot );
    return idmediaplayer;
}


bool isEyesLookAtTheScreen(){

}


void sendPauseAndPlay(int typeMediaPlayer){

    switch(typeMediaPlayer){

    HWND wm;
    case MEDIAPLAYERCLASSIC :
        wm = FindWindow(L"MediaPlayerClassicW",NULL);
        PostMessage(wm,0x111,(WPARAM)889,0);
        break;

    case WINDOWSMEDIAPLAYER :
        wm = FindWindow(L"WMPlayerApp",L"Windows Media Player");
        PostMessage(wm,0x111,(WPARAM)WMP_PAUSE,0);
        break;
    }
}
